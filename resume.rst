Paul-Emile MINY - Ingénieur/Développeur en informatique
=======================================================

+-------------------------------------------------------------------------+
|:Addresse : | 135 rue Marceau                                   |
|            | 91120 PALAISEAU                                                |
|:Tel. : +33(0)6 16 82 85 61                                              |
|:Email : paulemile.miny@gmail.com                                        |
|:Age: 36 ans                                                             |
+-------------------------------------------------------------------------+

FORMATION
---------
Diplôme d'ingénieur en électronique/informatique, option systèmes d'informations – ESEO – ANGERS (49)

COMPÉTENCES TECHNIQUES
----------------------
:Languages : Nodejs/Typescript/Javascript, PHP, Python, SQL, HTML5/CSS3/Sass
:Conception/Methodologies : UML, Design Patterns, TDD, Scrum (Kanban)
:Frameworks Web/CMS : Symfony 5, React/NextJS/Gatsby, VueJS, Drupal 8/7
:Outils : AWS services, GraphQL, Cypress/Jest, Docker, SaltStack/Ansible/Capistrano, Webpack, Gulp, Elasticsearch, RabbitMQ, Memcache, Redis

EXPERIENCES
-----------
:Septembre 2021 - Mai 2022  : | **Developpeur FullStack Senior – Wiis Digital (Freelance) – Paris (75)**
        | Conception et dévelopement d'un outil d'automatisation de gestion de document médicaux
        | Tech: NodeJs/Typescript GraphQL, CUPS, Prisma, Python opencv, tesseract

:Janvier 2020 - Décembre 2021  : | **Developpeur FullStack Senior – Cedric Grolet (Freelance) – Paris (75)**
        | Mise en place et maintenance d'un e-commerce spécialisé
        | Tech: React.js/Gatsby, AWS serverless (Lambda, DynamoDB, S3 ...), Stripe.

:Janvier 2020 - Mars 2021  : | **Developpeur FullStack Senior – Konbini (Freelance) – Paris (75)**
        | Refonte du site web konbini.com
        | Tech: React.js, NextJS, NodeJs/Typescript, GraphQL, Wordpress (api)

:Mars 2019 - Janvier 2020  : | **Lead developpeur Web – Phileas systems – Paris (75)**
        | Architecture et impémentation d'un système de gestion d'hotel (Gestion des reservations, des chambre, du personnel ...)
        | Particularité du projet : tout en serverless avec Amazon web services
        | Tech: React.js/Electon, Webpack, GraphQL, Node.js, Amazon webservices (Lambdas, DynamoDB, SNS, SQS, S3, ElasticSearch service, AppSync, StepFunctions, Cloudformation, CloudFront ...)
        | 
        
:Mai 2017 - Mars 2019 : | **Lead developpeur Web – Yoopies – Paris (75)**
        | Yoopies est un leader européan du babysitting.
        | Suivi de devs (PR review), veille et formations, maintenance/amélioration de plateforme (AWS).
        | Implémentation des nouvelles features et migration progressive de l'application vers une SPA (Vuejs, node) et GraphQL.
        | Tech: PHP /Symfony MariaDB, Redis, Varnish, Nginx, RabbitMQ, Vue.js, GraphQL, Webpack, Gulp
        | 
        
:Décembre 2016 - Mai 2017 : | **Lead developpeur Symfony/PHP – Fashion Network – Paris (75)**
        | Migration progressive du code legacy vers Symfony 3 et modernalisation.
        | Découpage du code legacy en project multiples Symfony 3 ou basé sur des composants Symfony.
        |
        
:Août 2012 - Novembre 2016 : | **Lead developpeur Symfony/PHP5/Drupal/Javascript – LaNetscouade – Paris (75)**
        | La Netscouade est une agence digitale de communication corporate.
        | Developpement d'applications Symfony2/3
        | Developpements Drupal
        | Developpement d'applications web riches ReactJs/AngularJs/Javascript
        |

:2010 - 2012  : | **Lead developpeur – lacompany – Montrouge (92)**
        | Suivi des travaux developpement
        | Mise en place d'outils de gestion de projet, de règles et de conventions de code
        | Gestion des serveurs de production (installation, mise à jour, backup, configuration, mise en production des projets)
        | Dévelopement backend et frontend – Symfony 2, symfony 1.4, HTML5, CSS3, javascript
        |

:2009 (6 mois) : | **Stage de fin d'étude – Développeur web PHP5 – Sensio labs – Clichy (91)**
                 | Maintenance, développement de projets courts sur le framework symfony 1.4 et PHP5
                 |

:2008 (4 mois) : | **Stage – Développeur web PHP5 – Ateme – Bièvres (92)**
                 | Développement d’un outil de gestion de configuration (LAMP) interne : gestion des produits ATEME, référencement, documentation, gestion de bugs, reporting
                 |


PROJETS RECENTS
---------------

:2019 : | **Lead Dev Web Phileas sytems**
        | Architecture et impémentation d'un système de gestion d'hotel (Gestion des reservations, des chambre, du personnel ...)
        | Particularité du projet : tout en serverless avec Amazon web services
        | Tech: React.js/Electon, Webpack, GraphQL, Node.js, Amazon webservices (Lambdas, DynamoDB, SNS, SQS, S3, ElasticSearch service, AppSync, StepFunctions, Cloudformation, CloudFront ...)
        |

:2017 - 2019 : | **Lead Dev Web yoopies.com**
        | `www.yoopies.com <http://www.yoopies.com>`_
        | Maintenance et évolution de la plateforme
        | Technologies / Outils: Aws Services, PHP Symfony, VueJs, Webpack, GraphQL ...
        |
        
:2017 : | **Lead Dev Web fashion-network.com**
        | `www.fashion-network.com <http://www.fashion-network.com>`_
        |


:2016 : | **Lead Dev refonte merignac.com**
        | `www.merignac.com <http://www.merignac.com>`_
        | Refonte en Drupal 7 de merignac.com
        | Technologies / Outils: Drupal 7, Elasticsearch
        |

        | **Lead Dev comparateur politique lasantecandidate.fr**
        | `lasantecandidate.fr <http://lasantecandidate.fr>`_
        | Technologies / Outils: React Js, Drupal 7
        |

:2015 : | **Lead Dev refonte lcp.fr**
        | `lcp.fr <http://www.lcp.fr>`_
        | Refonte en Drupal 7 de lcp.fr
        | Technologies / Outils: Drupal 7, Elasticsearch, Symfony, Memcache, Nginx
        |

        | **Lead Dev refonte bluelinkservices.com**
        | Technologies / Outils: Angular Js, Drupal 7
        |

:2014 : | **Lead Dev refonte service.civique.gouv.fr**
        | Refonte en Symfony 2 de service.civique.gouv.fr
        | Technologies / Outils: Symfony , ElasticSearch, Behat/PHPSpec/Travis, Redis, RabbitMQ, Gulp …
        |

        | **Lead Dev second écran pour Arte (FutureMag)**
        | Developpement d'un dispositif second écran
        | Technologies / Outils: Grunt, NodeJS/Javascript, Sass, Silex
        |

:2013 : | **Lead Dev web documentaire pour bforbank**
        | Developpement d'un web documentaire pour bforbank
        | Technologies / Outils: Grunt, Javascript, CSS3, Api Youtube
        |

        | **Lead Dev plateforme faire-simple.gouv.fr**
        | Developpement de la plateforme faire-simple.gouv.fr
        | Technologies / Outils: Drupal7, Solr, Bower, Composer
        |

        | **Lead Dev pour le développement du site Kedge Business School**
        | Developpement d'un site en Drupal pour un école de commerce
        | Technologies / Outils: Drupal7, CSS3
        |

:2012 : | **Plateforme de consultation**
        | Developpement d'une plateforme de consultation (Backend + API + Client)
        | Technologies / Outils: Symfony2, API REST, MySql, AngularJs, Behat, PHPUnit, Jenkins
        |

        | **Plateforme Ecommerce Dacast.com**
        | `www.dacast.com <http://www.dacast.com>`_
        | Developpement du noyau ecommerce, et des API REST, AMF associées
        | Technologies : Symfony2, Twig, Api REST, AMF, Redis, MySql, RabbitMQ
        |

        | **Rapport annuel 2011 – AXA**
        | `rapportannuel.axa.com <http://rapportannuel.axa.com>`_
        | Technologies : Silex, Twig, HTML5, CSS3, jQuery, coffeeScript, composer, Capistrano
        |

        | **Site d'information – Projet ligne Lyon-Turin – RFF**
        | `www.lyon-turin.info <http://www.lyon-turin.info>`_
        | Technologies : Drupal7, HTML5, SVG (RaphaëlJS), CSS3, jQuery
        |

:2011 : | **Site de l'agence W – W&Cie**
        | `www.wcie.fr <http://www.wcie.fr>`_
        | Technologies : Drupal7, HTML5, CSS3, jQuery
        |

        | **Site intitutionnel – Europassitance**
        | `www.europ-assistance.com <http://www.europ-assistance.com>`_
        | Technologies : Drupal7, HTML5, CSS3, jQuery
        |

        | **Blog – Esprit de Picardie – Région Picardie**
        | `lescarnets.espritdepicardie.com/ <http://lescarnets.espritdepicardie.com>`_
        | Technologies : Drupal6, HTML, CSS, jQuery
        |

        | **Boutique e-commerce**
        | Technologies : Symfony2, Twig, OAuth, Services REST, HTML5, CSS3, jQuery, Doctrine, Capistrano
        |

:2010 - 2012: | **Sites de stimulation de force de vente**
              | `stimit.com/ <http://stimit.com>`_
              | Technologies : symfony 1.4, Symfony2, Twig, HTML, CSS, jQuery, Doctrine, Capistrano
              | Clients: Colgate-Palmolive, HSBC, Nestlé, Barclay, Bel, Federal Mogul, Danone, Bouygues Telecom

